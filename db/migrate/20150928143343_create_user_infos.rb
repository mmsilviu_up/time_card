class CreateUserInfos < ActiveRecord::Migration
  def change
    create_table :user_infos do |t|
      t.string :email, required: true, uniqueness: true
      t.integer :quota, required:true
      t.boolean :user_active
      t.string :badge

      t.timestamps null: false
    end

    add_index :user_infos, :email
  end
end
