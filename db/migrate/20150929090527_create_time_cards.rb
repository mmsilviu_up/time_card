class CreateTimeCards < ActiveRecord::Migration
  def change
    create_table :time_cards do |t|
      t.belongs_to :time_type
      t.belongs_to :user_info, index: true

      t.integer :hours_worked, default: 0
      t.integer :quota_day, default: 0

      t.integer :leaving_time, default: 0
      t.integer :overtime, default: 0

      t.date :day
      t.boolean :on_call
      t.boolean :working_home
      t.string :comment

      t.timestamps null: false
    end
    add_index :time_cards, :day
  end
end
