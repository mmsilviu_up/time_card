class CreateMonthInfos < ActiveRecord::Migration
  def change
    create_table :month_infos do |t|
      t.date :month, required: true, uniqueness: true
      t.boolean :editable
      t.integer :quota

      t.timestamps null: false
    end
  end
end
