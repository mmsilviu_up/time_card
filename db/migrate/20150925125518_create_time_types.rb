class CreateTimeTypes < ActiveRecord::Migration
  def change
    create_table :time_types do |t|
      t.string :name, required: true
      t.string :short_name, required: true
      t.string :color, default: '#ffffff'

      t.timestamps null: false
    end
  end
end
