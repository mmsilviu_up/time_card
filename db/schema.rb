# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150929090527) do

  create_table "extra_times", force: :cascade do |t|
    t.integer  "time_card_id"
    t.string   "type"
    t.integer  "hours",        default: 0
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  add_index "extra_times", ["time_card_id"], name: "index_extra_times_on_time_card_id"

  create_table "month_infos", force: :cascade do |t|
    t.date     "month"
    t.boolean  "editable"
    t.integer  "quota"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "time_cards", force: :cascade do |t|
    t.integer  "time_type_id"
    t.integer  "user_info_id"
    t.integer  "hours_worked", default: 0
    t.integer  "quota_day",    default: 0
    t.integer  "leaving_time", default: 0
    t.integer  "overtime",     default: 0
    t.date     "day"
    t.boolean  "on_call"
    t.boolean  "working_home"
    t.string   "comment"
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  add_index "time_cards", ["day"], name: "index_time_cards_on_day"
  add_index "time_cards", ["user_info_id"], name: "index_time_cards_on_user_info_id"

  create_table "time_types", force: :cascade do |t|
    t.string   "name"
    t.string   "short_name"
    t.string   "color",      default: "#ffffff"
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
  end

  create_table "user_infos", force: :cascade do |t|
    t.string   "email"
    t.integer  "quota"
    t.boolean  "user_active"
    t.string   "badge"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "user_infos", ["email"], name: "index_user_infos_on_email"

end
