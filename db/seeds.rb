# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

TimeType.create(name: 'Concediu de odihna', short_name: 'Co', color: '#92d050')
TimeType.create(name: 'Concediu pentru evenimente speciale', short_name: 'Cs')
TimeType.create(name: 'Zile libere platite', short_name: 'L')
TimeType.create(name: 'Boala obisnuita', short_name: 'Bo')
TimeType.create(name: 'Deplasare în interes de serviciu (delegaţie)', short_name: 'D', color: '#ffc000')
TimeType.create(name: 'Zi libera legala', short_name: 'Zll')
TimeType.create(name: 'Invoiri si concedii fara retributie', short_name: 'I')
TimeType.create(name: 'Obligatii cetatenesti', short_name: 'Bp')
TimeType.create(name: 'Boala profesionala', short_name: 'L')
TimeType.create(name: 'Absente nemotivate', short_name: 'N')
TimeType.create(name: 'Accidente de munca', short_name: 'Am')
TimeType.create(name: 'Program redus maternitate', short_name: 'Prm')
TimeType.create(name: 'Maternitate', short_name: 'M')
TimeType.create(name: 'Program redus alaptare', short_name: 'Prb')
