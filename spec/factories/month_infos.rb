FactoryGirl.define do
  factory :month_info do

    trait :september do
      month Date.new(2015, 9, 1)
      editable  true
      quota 176
    end

  end

end
