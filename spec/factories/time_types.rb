FactoryGirl.define do
  factory :time_type do

    trait :with_co do
      short_name 'CO'
      name  'Concediu de odihna'
      color '#92d050'
    end

  end

end
