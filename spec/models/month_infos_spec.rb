require 'rails_helper'

describe MonthInfo, type: :model do

  describe '#new' do

    context 'with values' do
      let(:time_type) { FactoryGirl.build(:month_info, :september) }

      it 'is valid' do
        expect(time_type).to be_a MonthInfo
      end

      it 'month is september' do
        expect(time_type.month).to eq Date.new(2015,9,1)
      end

      it 'month is not september' do
        expect(time_type.month).not_to eq Date.new(2015,10,1)
      end

      it 'editable is true' do
        expect(time_type.editable).to eq true
      end

      it 'quota is 176' do
        expect(time_type.quota).to eql(176)
      end
    end

    context 'without values' do
      let(:time_type) { FactoryGirl.build(:month_info) }

      it 'is valid' do
        expect(time_type).to be_a MonthInfo
      end

      it 'month is september' do
        expect(time_type.month).not_to eql(Date.new(2015,9,1))
      end

      it 'editable is false' do
        expect(time_type.editable).to eql nil
      end

      it 'editable is true' do
        expect(time_type.editable).not_to eql(true)
      end

      it 'quota is 176' do
        expect(time_type.quota).not_to eql(176)
      end
    end

    context 'validation' do

      it 'fails without month' do
        expect(FactoryGirl.build(:month_info)).to have(1).error_on(:month)
      end

      it 'fails with quota < 100' do
        expect(FactoryGirl.build(:month_info)).to have(2).error_on(:quota)
        expect(FactoryGirl.build(:month_info, :quota => 100)).to have(1).error_on(:quota)
        expect(FactoryGirl.build(:month_info, :quota => 101)).to have(0).error_on(:quota)
      end

    end
  end

end
