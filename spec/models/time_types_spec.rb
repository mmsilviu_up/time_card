require 'rails_helper'

describe TimeType, type: :model do

  describe '#new' do

    context 'with values' do
      let(:time_type) { FactoryGirl.build(:time_type, :with_co) }

      it 'is valid' do
        expect(time_type).to be_a TimeType
      end

      it 'short name is CO' do
        expect(time_type.short_name).to eql('CO')
      end

      it 'short name different then CO' do
        expect(time_type.short_name).not_to eql('Zll')
      end

      it 'name is Concediu de odihna' do
        expect(time_type.name).to eql('Concediu de odihna')
      end

      it 'color is 92d050' do
        expect(time_type.color).to eql('#92d050')
      end
    end

    context 'without values' do
      let(:time_type) { FactoryGirl.build(:time_type) }

      it 'is valid' do
        expect(time_type).to be_a TimeType
      end

      it 'short name is CO' do
        expect(time_type.short_name).not_to eql('CO')
      end

      it 'name is Concediu de odihna' do
        expect(time_type.name).not_to eql('Concediu de odihna')
      end

      it 'color is 92d050' do
        expect(time_type.color).not_to eql('#92d050')
      end
    end

    context 'validation' do

      it 'fails without short name' do
        expect(FactoryGirl.build(:time_type, :short_name => '')).to have(2).error_on(:short_name)
      end

      it 'fails with short name length not between 1-3 characters' do
        expect(FactoryGirl.build(:time_type, :short_name => '')).to have(2).error_on(:short_name)
        expect(FactoryGirl.build(:time_type, :short_name => 'RRRR')).to have(1).error_on(:short_name)
        expect(FactoryGirl.build(:time_type, :short_name => 'CO')).to have(0).error_on(:short_name)
      end

      it 'fails without name' do
        expect(FactoryGirl.build(:time_type, :name => '')).to have(1).error_on(:name)
      end
    end
  end

end
