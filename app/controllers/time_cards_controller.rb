class TimeCardsController < ApplicationController

  before_action :set_time_card, only: [:show, :edit, :update, :destroy]

  # GET /time_cards
  # GET /time_cards.json
  def index

    @current_date = params[:selected_date]
    if !@current_date
      @current_date = Date.today
    else
      @current_date = Date.parse(@current_date)
    end

    @time_cards = TimeCard.where(day: @current_date.beginning_of_month..@current_date.end_of_month)
  end

  # GET /time_cards/1
  # GET /time_cards/1.json
  def show
    # @current_date = params[:id]
    # render index
  end


  # GET /time_cards/new
  def new
    _selected_day = params[:selected_day]
    @time_card = TimeCard.new(day: _selected_day)
  end

  # GET /time_cards/1/edit
  def edit
  end

  # POST /time_cards
  # POST /time_cards.json
  def create
    @time_card = TimeCard.new(time_card_params)

    respond_to do |format|
      if @time_card.save
        # format.html { redirect_to @time_card, notice: 'Time card was successfully created.' }
        # format.json { render :show, status: :created, location: @time_card }
        format.html { redirect_to time_cards_path, notice: 'Time card was successfully created.' }
        format.json { render :index, status: :created, location: time_cards_path }
      else
        format.html { render :new }
        format.json { render json: @time_card.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /time_cards/1D
  # PATCH/PUT /time_cards/1.json
  def update

    respond_to do |format|
      if @time_card.update(time_card_params)
        # format.html { redirect_to @time_card, notice: 'Time card was successfully updated.' }
        # format.json { render :show, status: :ok, location: @time_card }
        format.html { redirect_to time_cards_path, notice: 'Time card was successfully updated.' }
        format.json { render :index, status: :ok, location: time_cards_path }
      else
        format.html { render :edit }
        format.json { render json: @time_card.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /time_cards/1
  # DELETE /time_cards/1.json
  def destroy
    @time_card.destroy
    respond_to do |format|
      format.html { redirect_to time_cards_url, notice: 'Time card was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def populate
    _day = params[:day]
    #TODO set the quota instead of 8 hours
    @time_card = TimeCard.new(day: _day, hours_worked: 8, quota_day: 8, on_call:false, working_home:false)
    respond_to do |format|
      if @time_card.save
        format.html { redirect_to time_cards_path, notice: 'Time card was for ' + _day.to_s+ ' successfully created.' }
        format.json { render :index, status: :created, location: time_cards_path }
      else
        format.html { render :time_cards_path }
        format.json { render json: @time_card.errors, status: :unprocessable_entity }
      end
    end

  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_time_card
    @time_card = TimeCard.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def time_card_params
    params.require(:time_card).permit(:day, :time_type_id, :on_call, :working_home, :comment, :hours_worked, :leaving_time, :overtime, :quota_day)
  end
end
