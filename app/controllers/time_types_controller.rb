class TimeTypesController < ApplicationController
  before_action :set_time_type, only: [:show, :edit, :update, :destroy]

  # GET /time_types
  # GET /time_types.json
  def index
    @time_types = TimeType.all
  end

  # GET /time_types/1
  # GET /time_types/1.json
  def show
  end

  # GET /time_types/new
  def new
    @time_type = TimeType.new
  end

  # GET /time_types/1/edit
  def edit
  end

  # POST /time_types
  # POST /time_types.json
  def create
    @time_type = TimeType.new(time_type_params)

    respond_to do |format|
      if @time_type.save
        format.html { redirect_to @time_type, notice: 'Time type was successfully created.' }
        format.json { render :show, status: :created, location: @time_type }
      else
        format.html { render :new }
        format.json { render json: @time_type.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /time_types/1
  # PATCH/PUT /time_types/1.json
  def update
    respond_to do |format|
      if @time_type.update(time_type_params)
        format.html { redirect_to @time_type, notice: 'Time type was successfully updated.' }
        format.json { render :show, status: :ok, location: @time_type }
      else
        format.html { render :edit }
        format.json { render json: @time_type.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /time_types/1
  # DELETE /time_types/1.json
  def destroy
    @time_type.destroy
    respond_to do |format|
      format.html { redirect_to time_types_url, notice: 'Time type was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_time_type
      @time_type = TimeType.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def time_type_params
      params.require(:time_type).permit(:name, :short_name, :color)
    end
end
