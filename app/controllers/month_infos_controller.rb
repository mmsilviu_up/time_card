class MonthInfosController < ApplicationController
  before_action :set_month_info, only: [:show, :edit, :update, :destroy]

  # GET /month_infos
  # GET /month_infos.json
  def index
    @month_infos = MonthInfo.all
  end

  # GET /month_infos/1
  # GET /month_infos/1.json
  def show
  end

  # GET /month_infos/new
  def new
    @month_info = MonthInfo.new
  end

  # GET /month_infos/1/edit
  def edit
  end

  # POST /month_infos
  # POST /month_infos.json
  def create
    @month_info = MonthInfo.new(month_info_params)

    respond_to do |format|
      if @month_info.save
        format.html { redirect_to @month_info, notice: 'Month info was successfully created.' }
        # format.json { render :show, status: :created, location: @month_info }
      else
        format.html { render :new }
        # format.json { render json: @month_info.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /month_infos/1
  # PATCH/PUT /month_infos/1.json
  def update
    respond_to do |format|
      if @month_info.update(month_info_params)
        format.html { redirect_to @month_info, notice: 'Month info was successfully updated.' }
        # format.json { render :show, status: :ok, location: @month_info }
      else
        format.html { render :edit }
        # format.json { render json: @month_info.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /month_infos/1
  # DELETE /month_infos/1.json
  def destroy
    @month_info.destroy
    respond_to do |format|
      format.html { redirect_to month_infos_url, notice: 'Month info was successfully destroyed.' }
      # format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_month_info
      @month_info = MonthInfo.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def month_info_params
      params.require(:month_info).permit(:month, :editable, :quota)
    end
end
