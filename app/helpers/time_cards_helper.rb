module TimeCardsHelper


  # Time card has a time type?
  # @param [Object] time_card
  # @return Boolean
  def time_type_exist?(time_card)
    time_card.time_type.present?
  end

  def time_card_with_day(time_cards, day)
    time_cards.select{ |elem| elem.day == day}.first
  end
end
