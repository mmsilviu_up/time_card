json.array!(@time_cards) do |time_card|
  json.extract! time_card, :id, :day, :on_call, :working_home, :comment, :hours_worked, :quota_day
  json.url time_card_url(time_card, format: :json)
end
