json.array!(@time_types) do |time_type|
  json.extract! time_type, :id, :name, :short_name, :color
  json.url time_type_url(time_type, format: :json)
end
