json.array!(@month_infos) do |month_info|
  json.extract! month_info, :id, :month, :editable, :quota
  json.url month_info_url(month_info, format: :json)
end
