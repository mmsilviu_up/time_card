# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
#

pickerChangeHandler = (evt) ->
  rails_date = evt.date.year() + '-' + ('0' + (evt.date.month() + 1)).slice(-2) + '-' + ('0' + evt.date.date()).slice(-2)
##    $(this).next("input[type=hidden]").val(rails_date)
  console.log('New Date       ' +rails_date)
  if (evt.oldDate != null && (evt.date.year() != evt.oldDate.year() || evt.date.month() != evt.oldDate.month()) )
    window.location.href =  "/time_cards?selected_date=#{rails_date}"

$ ->
  $(document).on 'dp.change', '#datetimepicker10', pickerChangeHandler
