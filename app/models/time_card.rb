class TimeCard < ActiveRecord::Base
  belongs_to :time_type

  validates :day, :hours_worked, :quota_day, :presence => true
  validates_numericality_of  :hours_worked, :quota_day, :leaving_time, :overtime


  before_update do |time_card|
    #reset if time type was set
    if time_card.time_type.present?
      time_card.hours_worked = 0
      time_card.leaving_time = 0
      time_card.overtime = 0
    end
  end

end
