class TimeType < ActiveRecord::Base

  validates :name, :short_name, :presence => true
  validates_length_of :short_name, :maximum => 3, :minimum => 1
end
