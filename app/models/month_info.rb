class MonthInfo < ActiveRecord::Base
  validates :month, :quota, :presence => true
  validates_numericality_of :quota, greater_than: 100

end
