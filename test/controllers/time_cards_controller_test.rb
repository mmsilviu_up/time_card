require 'test_helper'

class TimeCardsControllerTest < ActionController::TestCase
  setup do
    @time_card = time_cards(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:time_cards)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create time_card" do
    assert_difference('TimeCard.count') do
      post :create, time_card: { comment: @time_card.comment, day: @time_card.day, hours_worked: @time_card.hours_worked, on_call: @time_card.on_call, quota_day: @time_card.quota_day, working_home: @time_card.working_home }
    end

    assert_redirected_to time_card_path(assigns(:time_card))
  end

  test "should show time_card" do
    get :show, id: @time_card
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @time_card
    assert_response :success
  end

  test "should update time_card" do
    patch :update, id: @time_card, time_card: { comment: @time_card.comment, day: @time_card.day, hours_worked: @time_card.hours_worked, on_call: @time_card.on_call, quota_day: @time_card.quota_day, working_home: @time_card.working_home }
    assert_redirected_to time_card_path(assigns(:time_card))
  end

  test "should destroy time_card" do
    assert_difference('TimeCard.count', -1) do
      delete :destroy, id: @time_card
    end

    assert_redirected_to time_cards_path
  end
end
