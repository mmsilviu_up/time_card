require 'test_helper'

class MonthInfosControllerTest < ActionController::TestCase
  setup do
    @month_info = month_infos(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:month_infos)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create month_info" do
    assert_difference('MonthInfo.count') do
      post :create, month_info: { editable: @month_info.editable, month: @month_info.month, quota: @month_info.quota }
    end

    assert_redirected_to month_info_path(assigns(:month_info))
  end

  test "should show month_info" do
    get :show, id: @month_info
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @month_info
    assert_response :success
  end

  test "should update month_info" do
    patch :update, id: @month_info, month_info: { editable: @month_info.editable, month: @month_info.month, quota: @month_info.quota }
    assert_redirected_to month_info_path(assigns(:month_info))
  end

  test "should destroy month_info" do
    assert_difference('MonthInfo.count', -1) do
      delete :destroy, id: @month_info
    end

    assert_redirected_to month_infos_path
  end
end
